package com.example.zhxl.customerview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;



/**
 * Created by Administrator on 2015/8/25.
 */
public class MyView extends View {
    public int x;
    public int y;
    private String test;
    private ImageView imageView;
    public MyView(Context context){
        super(context);

    }
    public MyView(Context context ,AttributeSet as){
        super(context,as);
    }
    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Paint p = new Paint();
        p.setColor(Color.GREEN);
        Bitmap bit = BitmapFactory.decodeResource(getResources(),R.drawable.c2b20150825_172815);
        float a = 10.1f;
        float b =10.2f;
        canvas.drawBitmap(bit,10.0f,10.0f,p);
    }
}
